import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * @author Ragulan Ramkumar (rr00663)
 */
public class Client {
  private static final String SERVER_IP = "127.0.0.1";
  private static final int SERVER_PORT = 60000;

  public static void main(String[] args) throws IOException, InterruptedException {
    String UUID;
    Socket socket = new Socket(SERVER_IP, SERVER_PORT);
    BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    BufferedReader userKeyboardInput = new BufferedReader(new InputStreamReader(System.in));
    PrintWriter output = new PrintWriter(socket.getOutputStream(), true);

    System.out.println("Connected to the server.\nSend HELLO to get a place in the queue.");
    while (true) {
      System.out.print("--> ");
      String userInput = userKeyboardInput.readLine();
      // Fix entering empty line crashing
      if (userInput.equals("")) {
        continue;
      }
      output.println(userInput);
      while (true) {
        // Interpret the messages here
        String response = input.readLine();
        if (response.equals("")) {
          break;
        }
        System.out.println("<-- From server: " + response);
        if (response.equals("UNKNOWN_COMMAND")) {
        }
        if (response.startsWith("UUID")) {
          UUID = response.substring(5, response.length());
          System.out.println("Your UUID is " + UUID + "\nYou'll need this to renter the queue later.");
        }
        if (response.equals("NEXT")) {
          System.out.println("It's your turn. Send DONE when complete.");
        }
        if (response.equals("GOODBYE")) {
          System.out.println("Closing connection");
          output.close();
          input.close();
        }
      }
    }
  }
}
