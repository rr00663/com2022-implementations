import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author Ragulan Ramkumar (rr00663)
 */
public class Server {
  private static ExecutorService threadPool = Executors.newFixedThreadPool(100);
  static List<ClientThread> clientQueue = new ArrayList<ClientThread>();
  static Map<String, ClientThread> UUIDMap = new HashMap<String, ClientThread>();
  private static final int PORT = 60000;

  public static void main(String[] args) throws IOException {
    ServerSocket listener = new ServerSocket(PORT);
    while (true) {
      System.out.println("[SERVER] WAITING FOR CONNECTION");
      Socket incomingClient = listener.accept();
      System.out.println("[SERVER] CLIENT CONNECTED!");
      ClientThread clientThread = new ClientThread(incomingClient);
      if (!clientQueue.isEmpty()) {
        System.out.println("CURRENTLY CONNECTED CLIENTS: ");
        for (int i = 0; i < clientQueue.size(); i++) {
          System.out.println("[" + i + "] " + clientQueue.get(i).toString());
        }
      }
      threadPool.execute(clientThread);
    }
  }
}
