import java.io.PrintWriter;
import java.io.BufferedReader;
import java.util.UUID;
import java.io.IOException;
import java.net.Socket;
import java.io.InputStreamReader;

/**
 * @author Ragulan Ramkumar (rr00663)
 */
public class ClientThread implements Runnable {
  private PrintWriter serverOutput = null;
  private Socket clientSocket = null;
  private UUID UUID = null;
  private BufferedReader clientInput = null;
  private ClientThread client = null;

  public ClientThread(Socket clientSocket) throws IOException {
    this.clientSocket = clientSocket;
    this.clientInput = new BufferedReader(new InputStreamReader(this.clientSocket.getInputStream()));
    this.serverOutput = new PrintWriter(clientSocket.getOutputStream(), true);
  }

  @Override
  public void run() {
    try {
      while (true) {
        String message = this.clientInput.readLine();
        message = message.toUpperCase();
        String response_code = message.split("\\s+")[0];
        switch (response_code) {
          case "HELLO":
            this.UUID = java.util.UUID.randomUUID();
            Server.UUIDMap.put(this.UUID.toString(), this);
            send("WELCOME");
            send("UUID " + this.UUID);
            Server.clientQueue.add(getClientObject());
            if (Server.clientQueue.size() == 1) { // only person in queue
              send("NEXT"); // send client straight to resource
            } else {
              // send position and add to queue
              send("POSITION " + getPosition());
            }
            send("");
            break;
          case "POSITION":
            send("POSITION " + getPosition());
            send("");
            break;

          case "UUID":
            String UUID = message.substring(5, message.length()).toLowerCase();
            if (Server.UUIDMap.containsKey(UUID)) {
              this.client = Server.UUIDMap.get(UUID);
              send("WELCOME_BACK");
              send("POSITION " + getPosition());
            } else {
              send("INCORRECT_UUID");
              send("Terminating connection");
              send("");
              serverOutput.close();
            }
            send("");
            break;

          case "GOODBYE":
            send("GOODBYE");
            send("");
            removeClientFromServer(getClientObject());
            serverOutput.close();
            break;

          case "DONE":
            if (Server.clientQueue.indexOf(getClientObject()) == 0) {
              send("Your connection is now being terminated.");
              send("GOODBYE");
              send("");
              removeClientFromServer(getClientObject());
            } else {
              send("It's not your turn. To exit the queue send GOODBYE.");
              send("");
            }
            break;

          default:
            send("UNKNOWN_COMMAND");
            send("");
        }
      }
    } catch (

    Exception e) {
      e.printStackTrace();
    } finally {
      serverOutput.close();
      try {
        clientInput.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
  }

  private ClientThread getClientObject() {
    return this.client == null ? this : this.client;
  }

  private int getPosition() {
    return Server.clientQueue.indexOf(getClientObject()) + 1;
  }

  private void send(String s) {
    serverOutput.println(s);
  }

  private void removeClientFromServer(ClientThread client) {
    Server.clientQueue.remove(client);
    Server.UUIDMap.remove(client.getUUID().toString());
  }

  public String getUUID() {
    return this.UUID.toString();
  }

  @Override
  public String toString() {
    return this.UUID == null ? "Unidentified Client" : this.UUID.toString();
  }
}
